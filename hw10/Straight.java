//
import java.util.Arrays;
public class Straight{
  //generate shuffled array method
  public static int[] card(int[] array){
    for(int i=0;i<array.length;i++){ //pick a random number to swap, get shuffled array
      int target=(int)(Math.random()*52-1);
      int temp=array[target];
      array[target]=array[i];
      array[i]=temp;
    }
    return array;
  }
  //first 5 cards method
  public static int[] firstFive(int []array){
    int[] newArray=new int[5];
    for(int i=0;i<5;i++){
      newArray[i]=array[i]%12; 
    }
    return newArray; //get first five cards from shuffled cards:(0-12)
  }
  //linear search 
  public static int Search(int[] newArray, int k){ 
    int num=0;//if it is a straight, plus   1
    if((k<=0)||(k>5)){
      System.out.println("Error! k is: "+k+" not in the range."); 
    }
    else{
      Arrays.sort(newArray);  //sort the array in order
      if((newArray[1]==(newArray[2]-1))&&(newArray[2]==(newArray[3]-1))&&(newArray[3]==(newArray[4]-1))&&(newArray[4]==(newArray[5]-1))){
        num++; //it is a straight
        
      } 
    }
    return num;
  }

  //main method
  public static void main(String[] args){
    int p=0; //count the number of straight
    for(int n=0;n<1000000;n++){ //perform this test for 1 million times
      int []array=new int[52]; 
      int[] cards=card(array);  //call method to generate a shuffled array
      int[] newArray=firstFive(cards); //call method to get frist five cards
      int k=1;
      int number=Search(newArray,k); //number is 1 if there is a straight in a test
       
      if(number==1){ 
        p+=1;
        break;
      }
    }
    System.out.println("There are "+p+" straights in 1 billions tests.");
  }
}
