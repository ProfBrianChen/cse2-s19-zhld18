//Robots invade the city
public class RobotCity{
  //build the city method
  public static int[][] buildCity(int size){
    int[][] city=new int[size][size];
    for(int i=0;i<size; i++){
      for(int j=0;j<size;j++){
        int population=(int)(Math.random()*900+99); //random population
        city[i][j]=population;
      }
    }
    return city;
  }
  //print method
  public static void display(int[][] city){
    for(int i=0;i<city.length;i++){
      for(int j=0;j<city[i].length;j++){
        System.out.printf("%d\t",city[i][j]);
      }
      System.out.println();
    }
  }
  //invade method to land robots on random blocks
  public static int[][] invade(int[][] city,int k){ //k is the number of invading robots
    for(int i=0;i<k;i++){
      int x=(int)((Math.random()*city.length)); //randomly select a place(i,j) for robot to land
      int y=(int)(Math.random()*city[0].length);
      if(city[x][y]>0){
        city[x][y]=(-1)*city[x][y]; //set to negative value
      }
    }
    return city;
  }
  //update method
  public static int[][] update(int city2){
    
    int [][] after=new int[city2.length][city2[0].length];
    for(int k=0;k< after.length;k++){
      for(int a=0;a< after[0].length;a++){
        if(city2[k][a]<0){
          after[k][a]=city2[k][a]*(-1); //negative becomes positive
        }
        else{
          after[k+1][a]=city2[k+1][a]*(-1); //eastward block positive becomes negative
        }
      }
    }
    return after;
  }
  //main method 
  public static void main(String[] args){
    //for(int n=0;n<5;n++){ //5 times
      int size=(int)(Math.random()*6+9); //random size between 10 to 15
      System.out.println(size);
      int[][] city=buildCity(size);
      display(city);
      int k=(int)(Math.random()*15); //the number of robots invade the city
      int[][] city2=invade(city,k); //began invade the city
      display(city2); 
      int[][] afterUpdate= update(city2);
      display(afterUpdate);
    //}
  }
}
