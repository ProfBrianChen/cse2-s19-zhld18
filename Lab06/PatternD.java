////This is a code to first ask the user to input an integer that is in [1,10], and then print out a parten 
////the length of this integer.
import java.util.Scanner;
public class PatternD{
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Type in an integer that is between 1 and 10: ");//ask the user to put in an integer that is between 1-10
//begain to check if the integer the user enter is between 1-10

//check if the number the user enter is an integer between 1 and 10
    boolean legit=false;
    int length=-1;
    do{
      if(myScanner.hasNextInt()){
        length=myScanner.nextInt();
        if((length>=1)&(length<=10)){
          legit=false;
          break;
        }
        else{
          legit=true;
          System.out.print("Error. Type in another integer which is between 1 and 10: ");
        }
      }
      else{
         myScanner.next();
         System.out.print("Error. Type in an integer:");
      }
    
    }while(legit==true);
 ////print out the pattern D
 ////numRow means how many number should be printed in each line, i means the value of the number
    for(int numRow=length;numRow>0;numRow--){
      for(int i=numRow; i>0;i--){
        System.out.print(i+" ");
      }
    System.out.println("");
    }
    
    
  }
}