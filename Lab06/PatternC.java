////This is a code to first ask the user to input an integer that is in [1,10], and then print out a parten 
////the length of this integer.
import java.util.Scanner;
public class PatternC{
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Type in an integer that is between 1 and 10: ");//ask the user to put in an integer that is between 1-10
//begain to check if the integer the user enter is between 1-10

//check if the number the user enter is an integer between 1 and 10
    boolean legit=false;
    int length=-1;
    do{
      if(myScanner.hasNextInt()){
        length=myScanner.nextInt();
        if((length>=1)&(length<=10)){
          legit=false;
          break;
        }
        else{
          legit=true;
          System.out.print("Error. Type in another integer which is between 1 and 10: ");
        }
      }
      else{
         myScanner.next();
         System.out.print("Error. Type in an integer:");
      }
    
    }while(legit==true);
 ////print out the pattern C 
    int r=1;////r represents which line your are in
    for(int i=length;i>0;i--){  ///how many lines shoule be printed
      
     ///print out triangle space
      for(int j=0;j<i-1;j++){
       System.out.print(" ");
      }
     ///print out number
    
     int num=r;
     for(int k=0;k<r;k++){
       System.out.print(num);
       num--;
     }
     System.out.println();
     ++r;
    }
   
  
    
   }
    
 }
