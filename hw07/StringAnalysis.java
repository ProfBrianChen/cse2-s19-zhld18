import java.util.Scanner;
public class StringAnalysis{
  //method for all check
  public static boolean check(String a){
    int length=a.length();//count how many letters are there in the entered word
    for(int n=1;n<=length;n++){
      char letter=a.charAt(n);//return to position n of the string
      if(letter >= 'a' && letter <= 'z'){
        System.out.println("All letter");
        return true;
      }
      else{
        System.out.print("It is not a letter at position "+(n+1));
        return false; 
      }
    }
    return true;
  }
  //method for check specified number of characters
  public static boolean check(int num,String b){ //num is how many characters needed to check
    for(int n=1;n<=num;n++){
      char letter=b.charAt(n); //return to the position n of the string
      if(letter >= 'a' && letter <= 'z'){
        System.out.println("All letter");
        return true;
      }
      else{
        System.out.print("It is not a letter at position "+n);
        return false;
        
      }
    }
    return true;
  }
  //main method
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Type in an String for examination: ");
    String word=myScanner.next();
    int length=word.length();
    System.out.print("If you want to exam all characters, enter Yes, else enter No: ");
    String answer=myScanner.next();
    if(answer.equals("Yes")){
      check(word);//invoke method for all check
      
    }
    else if(answer.equals("No")){
      System.out.print("How many letters you want to exam: ");
      int num=myScanner.nextInt();
      while(true){ //check if the number the user enter is smaller than the word's length
        if(num<=length){
          check(num,word);//invoke method for check specified number of characters
          //break;
        }
        else{
          System.out.print("Error.");
          myScanner.next();//delete the error, and ask the user to enter the number again
          System.out.print("Please enter a number that is smaller than the length of the word you entered: ");
        }
     }
    }
  }
}
