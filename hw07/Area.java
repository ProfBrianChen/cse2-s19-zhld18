//This code is to calculate the area of three shapes. Ask the user to enter the parameters of shapes and provide the result
import java.util.Scanner;
public class Area{
  //method for rectangle
  public static void areaRec(){
    double width,
           length;
    Scanner myScanner=new Scanner(System.in);
    
    System.out.print("Type in the width of the rectangle: ");
    width=myScanner.nextDouble();
    check(width);
    System.out.print("Type in the length of the rectangle: ");
    length=myScanner.nextDouble();
    check(length);
    System.out.print("The area of the rectangle is "+ width*length+".");
  }
  //method for triangle
  public static void areaTri(){
    double length,
           height;
    Scanner myScanner=new Scanner(System.in);
    
    System.out.print("Type in the height of the triangle: ");
    height=myScanner.nextDouble();
    check(height);
    System.out.print("Type in the length of the base of the triangle: ");
    length=myScanner.nextDouble();
    check(length);
    System.out.print("The area of the triangle is "+ (height*length)/2+".");//ask for the height
  }
  //method for circle
  public static void areaCir(){
    double radius,
           Area;
    Scanner myScanner=new Scanner(System.in);
    
    System.out.print("Type in the radius of the circle: ");
    radius=myScanner.nextDouble();
    check(radius);
    Area=3.14*radius*radius;
    System.out.print("The area of the circle is "+ Area+".");
  }
  //input method. To check whether the input number is double
  public static double check(double num){
    while(true){
      Scanner myScanner=new Scanner(System.in);
      if(myScanner.hasNextDouble()){
        return num;
      }
      else{
        myScanner.next();
        System.out.print("Error. Enter another double: ");
        return num;
      }
    }
   }

  //main method
 public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Type in one shape you want among rectangle, triangle and circle(without cap): ");
    String shape="";
    shape=myScanner.next();//scan in what the user want
    while(true){
      if(!(shape.equals("rectangle"))&&!(shape.equals("triangle"))&&!(shape.equals("circle"))){//check if the enter is a right shape
        System.out.print("Error. Please enter again: ");
        shape=myScanner.next();
      }
      else if(shape.equals("rectangle")){
        areaRec();
        break;
      }//check if its rectangle
      else if(shape.equals("triangle")){
        areaTri();
        break;
      }
      else if(shape.equals("circle")){
        areaCir();
        break;
      }
    }
    
  
  }
}