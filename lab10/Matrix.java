//This code is to generate a martix
import java.util.Arrays;
public class Matrix{
  //generate increasing matrix method 
  public static int [][] increasingMatrix(int width, int height, boolean format){
    if((width==0)||(height==0)){
      System.out.println("The array is empty!");
    }
    int[][] array=new int[height][width];
    if(format==true){ //generate row major matrix
      int a=1;
      for(int i=0;i<height;i++){
        for(int j=0;j<width;j++){
          array[i][j]=a;
          a+=1;
        }
      }
      System.out.println("Generating a matrix with width "+width+" and height "+height+":");
      printMatrix(array,true); //print the array
    }
    else{ //generate column major mathod
      int a=1;
      for(int j=0;j<width;j++){
        for(int i=0;i<height;i++){
          array[i][j]=a;
          a+=1;
        }
      }
      System.out.println("Generating a matrix with width "+width+" and height "+height+":");
      printMatrix(array,false); //print the array
    }
      return array;
  }
  //print method
  public static void printMatrix(int[][] array, boolean format){
    for(int i=0;i<array.length;i++){
      System.out.println(Arrays.toString(array[i]) +"\t");
    }
  }
  //translate method
  public static int[][]translate(int[][]array){
    int a=1;
    for(int i=0;i<array.length;i++){
        for(int j=0;j<array[i].length;j++){
          array[i][j]=a;
          a+=1;
         }
    }
    System.out.println("Translating colum major to row major input: ");
    return array;
  }
  // add method
  public static int[][] addMatrix(int [][]a,boolean formata,int height1,int width,int[][]b,boolean formatb,int height2){
    int[][] trans=new int[height2][];
    int[][] result=new int[height1][width]; //declare a new array to store result
    if((a.length==b.length)&&(height1==height2)){ //two matrix can be added 
      if (formatb==false){
        trans=translate(b); //translate column-major matrix b to row major
      }
      System.out.println("Output: ");

      for(int i=0;i<height1;i++){
        for(int j=0;j<width;j++){
          result[i][j]=a[i][j]+trans[i][j]; //add two matrix together
        }
      }
      printMatrix(result,true); //print out the result after adding
    }
    else{
      System.out.println("Unable to add input matrixs.");
    }
    return result;
  }
  //main method
  public static void main (String[] args){
    // first random width and height
    int width1=(int)(Math.random()*10);
    int height1=(int)(Math.random()*10);
    boolean formata=true; //matrixA is row major
    int [][]a=increasingMatrix(width1,height1,formata); //generate the arrayA
    int l=a.length;
    System.out.println(l);
    boolean formatb=false;// matrixB is column major
    int [][]b=increasingMatrix(width1,height1,formatb); //generate the arrayB
    //second random width and height
    int width2=(int)(Math.random()*10);
    int height2=(int)(Math.random()*10);
    boolean formatc=true; // matrixC is row majot
    int [][]c=increasingMatrix(width2,height2,formatc); //generate arrayC
    // add matrix A and B
    System.out.println("Adding two matrixs: ");
    printMatrix(a,true);
    System.out.println("plus");
    //printMatrix(b,false);
    addMatrix(a,formata,height1,width1,b,formatb,height1);
     // add matrix A and C
    System.out.println("Adding two matrixs: ");
    printMatrix(a,true);
    System.out.println("plus");
    printMatrix(c,true);
    System.out.println("Output: ");
    addMatrix(a,formata,height1,width2,c,formatc,height2);
  }
}