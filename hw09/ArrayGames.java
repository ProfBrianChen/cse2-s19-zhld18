//This code is to run insert or shorten game by using method and array
import java.util.Scanner;
import java.util.Arrays;
public class ArrayGames{
// print input method
public static void inPrint(int[] array){
  System.out.println("Input: "+Arrays.toString(array)); //print out array
}
//print output method
public static void ouPrint(int []array){
  System.out.println("Output: "+Arrays.toString(array)); //print out array
}

//method to generate array
public static int[] generate(){
  int size=(int)(Math.random()*11+10); //control the size of array between 10 and 20
  int[] array=new int[size];
  for(int i=0; i<array.length; i++){
    array[i]=(int)(Math.random()*10);
      //print out the array
  }
  inPrint(array);
  return array;
}

// insert method
public static int[] insert(int[]array1, int[]array2){
  int position=(int)(Math.random()*array1.length); //select a position to insert
  int length=array1.length+array2.length;
  int[] output= new int[length]; //declare new array 
  for(int i=0; i<=position; i++){ //first part of array1
    output[i] =array1[i];
  }
  int i = 0;
  for(int n= position+1; n<array2.length+position;n++){ 
    //insert whole array2 
    
    output[n]=array2[i++]; 
  }
  int p=position+1+array2.length;
  
  for(int m=p; m<output.length;m++){ //add remaining array1 at the end
    int q=m-array2.length;
    output[m]=array1[q];
  }
  ouPrint(output); //print out the outcome
  return output;
}

//shorten method
public static int[] shorten(int[]array, int index){
  System.out.print("Output: ");
  int[] output=new int[array.length];
  if(array.length<index){
    ouPrint(array); //print out output
  }
  else{ //index is smaller than array.length  //delete the index position 
    for(int i=0;i<array.length;i++){
      if(i==index){
        System.out.print("");
      }
      else{
        output[i]=array[i];
       System.out.print(output[i]+", ");
      } 
    }
  }
  return output;
}
//main method
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Run insert or shorten: ");
    String type=myScanner.next();
    if(type.equals("insert")){
      int[]input1=generate();
      int[] input2=generate();
      insert(input1, input2); //how can I put two different array into a method
      
    }
    else{
      int index=(int)(Math.random()*10);
      //invoke the method to generate an array
      //System.out.print("Output: ");
      shorten(generate(), index);
      System.out.println("");
      System.out.println("Index: "+index);
    }
  }
}