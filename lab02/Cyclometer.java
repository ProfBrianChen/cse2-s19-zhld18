/////////My bicycle cyclometer
//
public class Cyclometer {
      public static void main(String[] args) {
        int secsTrip1=480;  //the number of minutes for trip1
        int secsTrip2=3220;  //the number of minutes for trip2
	      int countsTrip1=1561;  //the number of counts for trip1
	      int countsTrip2=9037; //the number of counts for trip2
        double wheelDiameter=27.0,  // give value of wheelDiameter
       	PI=3.14159, // give value of PI
  	    feetPerMile=5280,  // give value of feetPerMile
      	inchesPerFoot=12,   // give value of inchesPerFoot
      	secondsPerMinute=60;  //give value of secondsPerMinute
	      double distanceTrip1, distanceTrip2,totalDistance;  // give value of distanceTrip1,distanceTrip2,totalDistance
        System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
        distanceTrip1=countsTrip1*wheelDiameter*PI; // gives distance in inches
	      distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
      	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
      	totalDistance=distanceTrip1+distanceTrip2;
        //Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	         System.out.println("Trip 2 was "+distanceTrip2+" miles");
         	System.out.println("The total distance was "+totalDistance+" miles");



      }
}