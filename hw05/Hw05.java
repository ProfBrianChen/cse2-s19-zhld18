////This code is to ask the user to enter information (the course number, department name,
////the number of times it meets in a week, the time the class starts, the instructor name,
////and the number of students) relating to a course they are currently taking.
import java.util.Scanner;
public class Hw05{
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);
    
////print out the course number
    System.out.print("Type in the course number: ");
    int courseNum=1;
    while(true){
      if(myScanner.hasNextInt()){
        courseNum=myScanner.nextInt();
        System.out.println("the course number is "+courseNum);
        break;
      }
      else{
        System.out.println("You type in an error");
        myScanner.next();
        System.out.print("Type in the course number: ");
      }
    }
    
////print out department name
    System.out.print("Type in the name of department: ");
    String departName=" ";
    while(true){
      if(myScanner.hasNext()){
        departName=myScanner.next();
        System.out.println("the department name is "+departName);
        break;
      }
      else{
        System.out.println("You type in an error");
        myScanner.next();
        System.out.print("Type in the name of department: ");
      }
    }
    
////print out the number of times it meets in a week
    System.out.print("Type in the number of times it meets in a week: ");
    int timesNeed=1;
    while(true){
      if(myScanner.hasNextInt()){
        timesNeed=myScanner.nextInt();
        System.out.println("the number of times it meets in a week is "+timesNeed);
        break;
      }
      else{
        System.out.println("You type in an error");
        myScanner.next();
        System.out.print("Type in the number of times it meets in a week: ");
      }
    }
    
////print out the time the class starts
    System.out.println("Type in the hour the class starts: ");
    int hour=1,
        minute=1;
    while(true){
      if(myScanner.hasNextInt()){
        hour=myScanner.nextInt();
        minute=myScanner.nextInt();
        if(((hour<24)&(hour>0))&((minute>0)&(minute<60))){
          System.out.println("the class starts at "+hour+":"+minute);
          break;
        }
        else{
          System.out.print("You type in an error. Type in the hour the class starts: ");
          myScanner.next();
          myScanner.next();   
          break;
        }
      }
    }
////print out the instructor's name
    System.out.print("Type in the instructor's name: ");
    String instructorNam=" ";
    while(true){
      if(myScanner.hasNext()){
        instructorNam=myScanner.next();
        System.out.println("the instructor is "+instructorNam);
        break;
      }
      else{
        System.out.println("You type in an error");
        myScanner.next();
        System.out.print("Type in the instructor's name: ");
      }
    }
    
 ////print out the number of students
    System.out.print("Type in the number of students: ");
    int studentNum=1;
    while(true){
      if(myScanner.hasNextInt()){
        studentNum=myScanner.nextInt();
        System.out.println("the number of student is "+studentNum);
        break;
      }
      else{
        System.out.println("You type in an error");
        myScanner.next();
        System.out.print("Type in the number of students: ");
      }
    }
  }  
}
