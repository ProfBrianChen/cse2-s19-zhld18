import java.util.Scanner;
//// This code is to calculate how much each person in the group need to pay in order to evenly split the cost.
public class Check{
    public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
      System.out.print("Enter the original cost of the check in the form xx.xx: ");
      double checkCost = myScanner.nextDouble();
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
      double tipPercent = myScanner.nextDouble();
      tipPercent /= 100; //We want to convert the percentage into a decimal value
      System.out.print("Enter the number of people who went out to dinner: ");
      int numPeople = myScanner.nextInt();
      int dollars,//whole dollar amount of cost
        dimes, //for storing digit
        pennies;//to the right of the decimal point
      double totalCost = checkCost * (1 + tipPercent);
      double costPerPerson = totalCost / numPeople;
      dollars=(int)costPerPerson; //get the whole amount, dropping decimal fraction
      dimes=(int)(costPerPerson * 10) % 10;
      pennies=(int)(costPerPerson * 100) % 10;
      System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies) ;

                         
}  //end of main method   
  	} //end of class

