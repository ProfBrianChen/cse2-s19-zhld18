////Convert meter to inches. After the user enter the meter, the program will convert the meter into inches and print out on the screen.
import java.util.Scanner;
public class Convert{
   public static void main(String[] args){
     Scanner myScanner=new Scanner( System.in);
     System.out.print("Enter the distance in meters: ");//
     double meters=myScanner.nextDouble();
     double inches=39.3701*meters;
     int singledigit,
         tenths,
         percentile,
         thousands,
         tenthousands;///to make the result have four decimals
     singledigit=(int)inches;
     tenths=(int)(inches*10)%10;
     percentile=(int)(inches*100)%10;
     thousands=(int)(inches*1000)%10;
     tenthousands=(int)(inches*10000)%10;
       System.out.println(meters+" meters is "+singledigit+'.'+tenths+percentile+thousands+tenthousands+" inches.");
     
   
   }
}