////The program would help the user to calculate the volume inside the box by input the width, length, height.
import java.util.Scanner;
public class BoxVolume{
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("The width side of the box is: ");//input width value
    double width=myScanner.nextDouble();
    System.out.print("The length of the box is: ");//input length value
    double length=myScanner.nextDouble();
    System.out.print("The height of the box is: ");//input height value
    double height=myScanner.nextDouble();
    double volume=height*length*width;//function to get volume of the box
    int Volume;
    Volume=(int)volume;//round to integer
    System.out.println("The volume inside the box is: "+Volume);//print out the answer
    
  }
  
}