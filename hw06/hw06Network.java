////This is a program that displays a window into a “network” of boxes connected by lines. 
import java.util.Scanner;
public class hw06Network{
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Type in the width of the window: ");
 //check if the number entered is a positive integer
    boolean legit=false;
    int width=0,
        height=0,
        sqaureSize=0,
        edgeLength=0;
    do{
      if(myScanner.hasNextInt()){
        width=myScanner.nextInt();
        if(width>0){
          legit=false;
          System.out.println("Input your desired width: "+width);
          break;
        }
        else{
          legit=true;
          System.out.println("Error.The width should be positive. Please enter again: "); 
        }
      }
      else{
        myScanner.next();
        System.out.println("Error. The width should be an integer. Please enter again: ");
      }
    }while(legit==true);
    
 ////check if height is positive integer
    System.out.print("Type in the height of the window: ");
    do{
      if(myScanner.hasNextInt()){
        height=myScanner.nextInt();
        if(height>0){
          legit=false;
          System.out.println("Input your desired height: "+height);
          break;
        }
        else{
          legit=true;
          System.out.println("Error.The height should be positive. Please enter again: "); 
        }
      }
      else{
        myScanner.next();
        System.out.println("Error. The height should be an integer. Please enter again: ");
      }
    }while(legit==true);
    
////check if size is a positive integer
    System.out.print("Type in the size of the sqaure: ");
    do{
      if(myScanner.hasNextInt()){
        sqaureSize=myScanner.nextInt();
        if(sqaureSize>0){
          legit=false;
          System.out.println("Input your desired sqaureSize: "+sqaureSize);
          break;
        }
        else{
          legit=true;
          System.out.println("Error.The sqaureSize should be positive. Please enter again: "); 
        }
      }
      else{
        myScanner.next();
        System.out.println("Error. The sqaureSize should be an integer. Please enter again: ");
      }
    }while(legit==true);   
    
    ////check if length is a positive integer
    System.out.print("Type in the length of the edge: ");
    do{
      if(myScanner.hasNextInt()){
        edgeLength=myScanner.nextInt();
        if(edgeLength>0){
          legit=false;
          System.out.println("Input your desired edgeLength: "+edgeLength);
          break;
        }
        else{
          legit=true;
          System.out.println("Error.The edgeLength should be positive. Please enter again: "); 
        }
      }
      else{
        myScanner.next();
        System.out.println("Error. The edgeLength should be an integer. Please enter again: ");
      }
    }while(legit==true);
    
////print out the pattern
      int i=1;
      int a;
      int heightPosition=1; 
      int widthPosition;
      int edgePosition;//record the position where should print edge
    
    a=edgeLength+sqaureSize;//""a is how long is a group in each line
    
    do{ 
      
      for(int j=1;j<=width;j++){ //j represent which colome in printing
        widthPosition=j%a; //assaign position number to each position in each line
        if((widthPosition<=sqaureSize)&(widthPosition>0)){ //distinguish if the position is in "box" area or "edge" area. This is in box area
          for( i=1;i<=height;i++){   //i represent which line is printing
            heightPosition=i%a;
            if((heightPosition==1)|(heightPosition==6)){ //when the position is the side of the box. Should print out # and -
              if((widthPosition==1)|(widthPosition==6)){ //print out # on the side of the box.
                System.out.print("#");
                break;
              }
              else{System.out.print("-"); 
              }
              break;
            }
            else{ //when the position is at the middle part of the box. Should print out "|" and " "
              if((widthPosition==1)|(widthPosition==6)){ //print out | in the middle part of the box
                System.out.print("|");
                break;
              }
              else{  //print out " " in the middle part of the box
                System.out.print(" ");
              }
            }
          } 
        break;
        }
        else{ // the position is in edge area
          if(sqaureSize%2==0){ //check if the size of square is even, then print double width of edge
            edgePosition=sqaureSize/2; //find out where should print the edge
            if((heightPosition==edgePosition)|(heightPosition==edgePosition+1)){ //print out - in the edge
              System.out.print("-");
              break;
            }
            else{System.out.print(" "); //print out " " in the edge 
            }
          break; 
          }
          else if(sqaureSize%2==1){ //if the size of the square is odd
            edgePosition=(sqaureSize+1)/2; //find out where should print the edge
            if(heightPosition==edgePosition){
              System.out.print("-"); //print out single line of "-" 
              break;
            }
            else{System.out.print(" "); //print out " " in the edge 
            }
          }
        }
      }
      
    }while(i<=height);//the outtest loop
   
  }
}