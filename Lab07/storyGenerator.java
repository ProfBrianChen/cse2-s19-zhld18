////This code is to generate a random artificial stroy using method
import java.util.Random;
import java.util.Scanner;
public class storyGenerator{
  //esrablish a method to randomly select an adjectives.
  public static String Adjectives(int a){
    String adjectives="";
    switch(a){
      case 0: adjectives="cute";break;
      case 1: adjectives="fat";break;
      case 2:adjectives="swift";break;
      case 3:adjectives="beautiful";break;
      case 4:adjectives="lazy";break;
      case 5: adjectives="colorful";break;
      case 6:adjectives="tall"; break;
      case 7:adjectives="powerful";break;
      case 8:adjectives="terrible"; break;
      case 9: adjectives="slow";break;
    }
    return adjectives;
  }
  //establish a method to randomly select a Non-primary nouns appropriate for the subject of a sentence
  public static String NounSub(int b){
    String nounSub="";
    switch(b){
      case 0: nounSub="cat";break;
      case 1: nounSub="dog";break;
      case 2:nounSub="dolphin";break;
      case 3:nounSub="bird";break;
      case 4:nounSub="turtel";break;
      case 5:nounSub="fish";break;
      case 6:nounSub="girrafe"; break;
      case 7:nounSub="pig";break;
      case 8:nounSub="bear"; break;
      case 9:nounSub="shrimp";break;
    }
    return nounSub;
  }
  //establish a method to create a random past-tense verb
  public static String Verb(int c){
    String verb="";
    switch(c){
      case 0:verb="hit";break;
      case 1: verb="ate";break;
      case 2:verb="loved";break;
      case 3: verb="chased";break;
      case 4: verb="passed";break;
      case 5: verb="dived into";break;
      case 6: verb="run"; break;
      case 7:verb="sleeped on";break;
      case 8:verb="bited"; break;
      case 9: verb="played";break;
    }
    return verb;
  }
  //establish a method to create a random Non-primary nouns appropriate for the object of the sentence
  public static String NounObej(int d){
    String nounObej="";
    switch(d){
      case 0: nounObej="apple";break;
      case 1: nounObej="peach";break;
      case 2: nounObej="water";break;
      case 3:nounObej="nut";break;
      case 4:nounObej="rice";break;
      case 5: nounObej="chocolate";break;
      case 6: nounObej="milk"; break;
      case 7: nounObej="bread";break;
      case 8: nounObej="ice"; break;
      case 9:nounObej="chili";break;
    }
    return nounObej;
  }
  //start the main method
  public static void main(String[] args){
    //randomly select 4 number
    Random randomGenerator= new Random();
    boolean legit=true;
  do{
    int randomInt1=randomGenerator.nextInt(10);
    int a=randomInt1;
    int randomInt2=randomGenerator.nextInt(10);
    int b=randomInt2;
    int randomInt3=randomGenerator.nextInt(10);
    int c=randomInt3;
    int randomInt4=randomGenerator.nextInt(10);
    int d=randomInt4;
    int randomInt5=randomGenerator.nextInt(10);
    int f=randomInt5;
    
    //invoke four methods and print them out as the first sentence.
    System.out.println("The "+Adjectives(a)+" "+NounSub(b)+" "+Verb(c)+" the "+Adjectives(f)+" "+NounObej(d)+".");
    //ask if the user wants to create another sentence.
    String answer="";
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Want another sentence? If want enter Yes, else enter No: ");
    answer=myScanner.next();
    
    if (answer=="Yes"){legit=true;}
    else{legit=false;break;}
    
  } while(legit=true);
  //print out the second sentence
    int randomInt6=randomGenerator.nextInt(10);
    int g=randomInt6;
    int randomInt7=randomGenerator.nextInt(10);
    int h=randomInt7;
    System.out.println("It likes to "+Verb(g)+" "+NounObej(h)+".");
    //print out the conclusion
     int randomInt2=randomGenerator.nextInt(10);
    int b=randomInt2;
    int randomInt8=randomGenerator.nextInt(10);
    int i=randomInt8;
    int randomInt9=randomGenerator.nextInt(10);
    int j=randomInt9;
    int randomInt10=randomGenerator.nextInt(10);
    int k=randomInt10;
    System.out.println("That "+ NounSub(b)+" "+Verb(j)+" her "+NounObej(k)+"!");
  } 
}