////This code is for generating five randomly selected number and define them as 
////"a pair""two pair" "three of a kind" or"high card hand"
//1-13 diamonds
//14-26 clubs
//27-39 hearts
//40-52 spades
public class PokerHandCheck{
  public static void main(String[] args){
    
    int randomNum1=(int)(Math.random()*52+1);//select random number from 1 to 52.
    
    String suitName="0",
           cardIdentity="0";
    System.out.println("Your random cards were:");
    if ((1<=randomNum1)&&(randomNum1<=13)){
       suitName="Diamonds";
    }
      else if ((14<=randomNum1)&&(randomNum1<=26)){
        suitName="Clubs";
      }
      else if ((27<=randomNum1)&&(randomNum1<=39)){
        suitName="Hearts";
      }
      else if ((40<=randomNum1)&&(randomNum1<=52)){
        suitName="Spades";
      }
      
      int result1;
      result1 =randomNum1%13;
    //sort card identity
      switch(result1){
        case 1:
              cardIdentity="Aces";
           break;
        case 11:
              cardIdentity="Jack";
          break;
        case 12:
          cardIdentity="Queen";
          break;
        case 0:
            cardIdentity="King";
            break;
        case 2:
          cardIdentity="2";
          break;
        case 3:
          cardIdentity="3";
          break;
        case 4:
          cardIdentity="4";
          break;
        case 5:
          cardIdentity="5";
          break;
        case 6:
          cardIdentity="6";
          break;
        case 7:
          cardIdentity="7";
          break;
        case 8:
          cardIdentity="8";
          break;
        case 9:
          cardIdentity="9";
          break;
        case 10:
          cardIdentity="10";
          break;

        }
    System.out.println("the "+ cardIdentity+ " of "+suitName);
    
///select the second card   
     int randomNum2=(int)(Math.random()*52+1);//select random number from 1 to 52.
    
    if ((1<=randomNum2)&&(randomNum2<=13)){
       suitName="Diamonds";
    }
      else if ((14<=randomNum2)&&(randomNum2<=26)){
        suitName="Clubs";
      }
      else if ((27<=randomNum2)&&(randomNum2<=39)){
        suitName="Hearts";
      }
      else if ((40<=randomNum2)&&(randomNum2<=52)){
        suitName="Spades";
      }
      
      int result2;
      result2 =randomNum2%13;
    //sort card identity
      switch(result2){
        case 1:
              cardIdentity="Aces";
           break;
        case 11:
              cardIdentity="Jack";
          break;
        case 12:
          cardIdentity="Queen";
          break;
        case 0:
            cardIdentity="King";
            break;
        case 2:
          cardIdentity="2";
          break;
        case 3:
          cardIdentity="3";
          break;
        case 4:
          cardIdentity="4";
          break;
        case 5:
          cardIdentity="5";
          break;
        case 6:
          cardIdentity="6";
          break;
        case 7:
          cardIdentity="7";
          break;
        case 8:
          cardIdentity="8";
          break;
        case 9:
          cardIdentity="9";
          break;
        case 10:
          cardIdentity="10";
          break;
        }
System.out.println("the "+ cardIdentity+ " of "+suitName);    
///select the third card   
     int randomNum3=(int)(Math.random()*52+1);//select random number from 1 to 52.
    
    if ((1<=randomNum3)&&(randomNum3<=13)){
       suitName="Diamonds";
    }
      else if ((14<=randomNum3)&&(randomNum3<=26)){
        suitName="Clubs";
      }
      else if ((27<=randomNum3)&&(randomNum3<=39)){
        suitName="Hearts";
      }
      else if ((40<=randomNum3)&&(randomNum3<=52)){
        suitName="Spades";
      }
      
      int result3;
      result3 =randomNum3%13;
    //sort card identity
      switch(result3){
        case 1:
              cardIdentity="Aces";
           break;
        case 11:
              cardIdentity="Jack";
          break;
        case 12:
          cardIdentity="Queen";
          break;
        case 0:
            cardIdentity="King";
            break;
        case 2:
          cardIdentity="2";
          break;
        case 3:
          cardIdentity="3";
          break;
        case 4:
          cardIdentity="4";
          break;
        case 5:
          cardIdentity="5";
          break;
        case 6:
          cardIdentity="6";
          break;
        case 7:
          cardIdentity="7";
          break;
        case 8:
          cardIdentity="8";
          break;
        case 9:
          cardIdentity="9";
          break;
        case 10:
          cardIdentity="10";
          break;
        }
System.out.println("the "+ cardIdentity+ " of "+suitName);    
///select the fourth card   
     int randomNum4=(int)(Math.random()*52+1);//select random number from 1 to 52.
    
    if ((1<=randomNum4)&&(randomNum4<=13)){
       suitName="Diamonds";
    }
      else if ((14<=randomNum4)&&(randomNum4<=26)){
        suitName="Clubs";
      }
      else if ((27<=randomNum4)&&(randomNum4<=39)){
        suitName="Hearts";
      }
      else if ((40<=randomNum4)&&(randomNum4<=52)){
        suitName="Spades";
      }
      
      int result4;
      result4 =randomNum4%13;
    //sort card identity
      switch(result4){
        case 1:
              cardIdentity="Aces";
           break;
        case 11:
              cardIdentity="Jack";
          break;
        case 12:
          cardIdentity="Queen";
          break;
        case 0:
            cardIdentity="King";
            break;
        case 2:
          cardIdentity="2";
          break;
        case 3:
          cardIdentity="3";
          break;
        case 4:
          cardIdentity="4";
          break;
        case 5:
          cardIdentity="5";
          break;
        case 6:
          cardIdentity="6";
          break;
        case 7:
          cardIdentity="7";
          break;
        case 8:
          cardIdentity="8";
          break;
        case 9:
          cardIdentity="9";
          break;
        case 10:
          cardIdentity="10";
          break;
        }
System.out.println("the "+ cardIdentity+ " of "+suitName);    
///select the fifth card   
     int randomNum5=(int)(Math.random()*52+1);//select random number from 1 to 52.
    
    if ((1<=randomNum5)&&(randomNum5<=13)){
       suitName="Diamonds";
    }
      else if ((14<=randomNum5)&&(randomNum5<=26)){
        suitName="Clubs";
      }
      else if ((27<=randomNum5)&&(randomNum5<=39)){
        suitName="Hearts";
      }
      else if ((40<=randomNum5)&&(randomNum5<=52)){
        suitName="Spades";
      }
      
      int result5;
      result5 =randomNum5%13;
      switch(result5){
        case 1:
              cardIdentity="Aces";
           break;
        case 11:
              cardIdentity="Jack";
          break;
        case 12:
          cardIdentity="Queen";
          break;
        case 0:
            cardIdentity="King";
            break;
        case 2:
          cardIdentity="2";
          break;
        case 3:
          cardIdentity="3";
          break;
        case 4:
          cardIdentity="4";
          break;
        case 5:
          cardIdentity="5";
          break;
        case 6:
          cardIdentity="6";
          break;
        case 7:
          cardIdentity="7";
          break;
        case 8:
          cardIdentity="8";
          break;
        case 9:
          cardIdentity="9";
          break;
        case 10:
          cardIdentity="10";
          break;
        }
       System.out.println("the "+ cardIdentity+ " of "+suitName);   
    
//Sort "a pair""two pair" "three of a kind" or"high card hand"
   int c=0;
    if (result1==result2){
      c=c+1;
    }
    if (result1==result3){
        c=c+1;
      }
    if (result1==result4){
          c=c+1;
      }
      if (result1==result5){
            c=c+1;
      }
      if (result2==result3){
              c=c+1;
      }
      if (result2==result4){
                c=c+1;
      }
      if (result2==result5){
                  c=c+1;
      }
      if (result3==result4){
                    c=c+1;
      }
       if (result3==result5){
                      c=c+1;
      }
      if (result4==result5){
                        c=c+1;
      }
    
    if (c==1){
      System.out.println("You have a pair!");
    }
      else if (c==0){
        System.out.println("You have a high card hand!");
      }
    
      else if((c==2)||(c==6)){
        System.out.println("You have two pairs!");
      }
      else if(c==3){
        System.out.println("You have three of a kind!");
      }
      else if (c==4){
        System.out.println("You have three of a kind and a pair!");
      }
   
  
  }
}