//This code is for users to play lottery
import java.util.Scanner;
public class PlayLottery{
  
  //method to generate random number for lottery
  public static int[] numbersPicked(){
    int[] lotNum=new int[5]; //declare an array called lotNum to store the 5 random numbers
    for(int j=0;j<5;j++){
      int num=(int)(Math.random()*60); //generate the lotterywin number from 0 to 59
      lotNum[j]=num; //store lotterywin number in the array
    }
    return lotNum; //return the array to the main method
  }
  
  //method to compare if the player is win or not
  public static boolean win(int[] user,int[] lotNum){
    for(int h=0;h<5;h++){
      if(user[h]==lotNum[h]){
        System.out.print("You win");
        return true;
      }
      else{
        System.out.print("You lose");
        return false;
      }
    }
    return true;
  }
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in);
    System.out.print("Please enter 5 integer: ");
    int[] user=new int[5];//declare an array called user 
    for(int i=0;i<5;i++){ //store 5 int into an array
      user[i]=myScanner.nextInt();
      
    }
    int[] lotNum=numbersPicked(); //invoke the method to generate 5 randomly lottery number
    win(user,lotNum);  //invoke the method to check if the player win
    
  }
}
