//This code is to generate random upper and lower case letters and sort them into two groups
import java.util.Random;
public class Letters{
  //method getAtoM
  public static char[] getAtoM(char[] letter){
    System.out.println("A to M characters: ");
    int size = 0; //check how many atom letters 
    for(int j=0;j<letter.length;j++){
      if(letter[j]<='m'&&'a'<=letter[j]  || letter[j]<='M'&&'A'<=letter[j]){
          size++;
      }
    }
    char[] result=new char[size];
    for(int j=0;j<letter.length;j++){
      int k = 0;
      if(letter[j]<='m'&&'a'<=letter[j]  || letter[j]<='M'&&'A'<=letter[j]){
        result[k] = letter[j];
        System.out.println(result[k]);
        k++;
      }
    }
    
    return result;
  }
  
  //method get N to Z
  public static char[] getNtoZ(char[]letter){
    System.out.println("N to Z characters: ");
    int size = 0; //check how many ntoz letters
    for(int j=0;j<letter.length;j++){
      if(letter[j]<='z'&&'n'<=letter[j]  || letter[j]<='Z'&&'N'<=letter[j]){
          size++;
      }
    }
   char[] result=new char[size];
    for(int j=0;j<letter.length;j++){
      int k = 0;
      if(letter[j]<='z'&&'n'<=letter[j]  || letter[j]<='Z'&&'N'<=letter[j]){
        result[k] = letter[j];
        System.out.println(result[k]);
        k++;
      }
    }
    
    return result;
  }
  
  //main method
  public static void main(String[] args){
    int size=(int)(Math.random()*10);//generate a random size
    char[] letter=new char[size];  //declare a new array
    System.out.println("Random characters array: ");
    for(int i=0;i<letter.length;i++){ 
      Random r = new Random();
      int positon=(int)(Math.random()*52+1);
      if(positon<=26){
        char lowerc = (char)(r.nextInt(26) + 'a'); //randomly select lowercase letter
        System.out.println(lowerc);
        letter[i]=lowerc; //store char to the array
      }
      else{
        char upperc=(char)(r.nextInt(26)+'A'); //randomly select uppercase letter
        System.out.println(upperc);
        letter[i]=upperc;  //store the char to the array
      }
    }
    //sort A to m
    //invoke the getAtoM method
    getAtoM(letter);
 
    //sort n to z
    //invoke the getNtoZ method
    getNtoZ(letter);
    
  }
}
